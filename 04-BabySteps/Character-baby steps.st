'From Cuis 6.0 [latest update: #5481] on 29 September 2022 at 9:34:01 pm'!


!Character methodsFor: 'baby steps' stamp: 'MF 9/29/2022 18:56:35'!
asRot13

	self asciiValue < $A asciiValue ifTrue: [ ^self ].
	self asciiValue < $N asciiValue ifTrue: [ ^self class asciiValue: (self asciiValue + 13) ].
	self asciiValue <= $Z asciiValue ifTrue: [ ^self class asciiValue: (self asciiValue - 13) ].
	self asciiValue < $a asciiValue ifTrue: [ ^self ].
	self asciiValue < $n asciiValue ifTrue:  [ ^self class asciiValue: (self asciiValue + 13) ].
	self asciiValue <= $z asciiValue ifTrue: [ ^self class asciiValue: (self asciiValue - 13) ].
	^self! !

!Character methodsFor: 'baby steps' stamp: 'MF 9/29/2022 18:13:41'!
between: firstCharacter and: lastCharacter 
	^self asciiValue between: (firstCharacter asciiValue) and: (lastCharacter asciiValue).! !
