!classDefinition: #I category: 'NaturalNumbers'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'NaturalNumbers'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'next' stamp: 'ARM 8/25/2022 20:43:03'!
next
	^II! !


!I class methodsFor: 'operations' stamp: 'MF 8/27/2022 21:07:08'!
* anProduct

	^anProduct! !

!I class methodsFor: 'operations' stamp: 'ARM 8/25/2022 21:12:30'!
+ anAdder

	^anAdder next! !

!I class methodsFor: 'operations' stamp: 'ds 8/30/2022 16:09:51'!
- subtrahend
	self error: self negativeNumbersNotSupportedErrorDescription! !

!I class methodsFor: 'operations' stamp: 'ds 8/30/2022 16:37:18'!
/ aDivisor
	aDivisor == I ifTrue: [^I] ifFalse: [self error: II canNotDivideByBiggerNumberErrorDescription ]! !

!I class methodsFor: 'operations' stamp: 'ds 8/29/2022 20:38:06'!
substractFrom: minuend
	^minuend previous! !


!I class methodsFor: 'error' stamp: 'ds 8/30/2022 16:07:25'!
negativeNumbersNotSupportedErrorDescription
	^'Negative numbers not supported'! !


!I class methodsFor: 'comparators' stamp: 'ds 8/30/2022 17:18:51'!
< numberToCompare
	^numberToCompare ~~ I! !


!classDefinition: #II category: 'NaturalNumbers'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'NaturalNumbers'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'next & previous' stamp: 'ds 8/29/2022 21:32:02'!
nameOfNext
	(self name endsWith: 'CMXCIX') ifTrue: [^(self name withoutSuffix: 'CMXCIX'), 'M'].
	(self name endsWith: 'DCCCXCIX') ifTrue: [^(self name withoutSuffix: 'DCCCXCIX'), 'CM'].
	(self name endsWith: 'CDXCIX') ifTrue: [^(self name withoutSuffix: 'CDXCIX'), 'D'].
	(self name endsWith: 'CCCXCIX') ifTrue: [^(self name withoutSuffix: 'CCCXCIX'), 'CD'].
	(self name endsWith: 'XCIX') ifTrue: [^(self name withoutSuffix: 'XCIX'), 'C'].
	(self name endsWith: 'LXXXIX') ifTrue: [^(self name withoutSuffix: 'LXXXIX'), 'XC'].
	(self name endsWith: 'XLIX') ifTrue: [^(self name withoutSuffix: 'XLIX'), 'L'].
	(self name endsWith: 'XXXIX') ifTrue: [^(self name withoutSuffix: 'XXXIX'), 'XL'].
	(self name endsWith: 'IX') ifTrue: [^(self name withoutSuffix: 'IX'), 'X'].
	(self name endsWith: 'VIII') ifTrue: [^(self name withoutSuffix: 'VIII'), 'IX'].
	(self name endsWith: 'IV') ifTrue: [^(self name withoutSuffix: 'IV'), 'V'].
	(self name endsWith: 'III') ifTrue: [^(self name withoutSuffix: 'III'), 'IV'].
	^ self name, 'I'! !

!II class methodsFor: 'next & previous' stamp: 'ds 8/29/2022 21:16:23'!
next
	next ifNil:[
		next _ II createChildNamed: self nameOfNext.
		next previous: self.
		].
	^next! !

!II class methodsFor: 'next & previous' stamp: 'ARM 8/25/2022 21:21:12'!
previous

	^previous! !

!II class methodsFor: 'next & previous' stamp: 'ARM 8/25/2022 21:25:57'!
previous: aNumber 

	previous := aNumber! !


!II class methodsFor: 'remove all next' stamp: 'ARM 8/25/2022 21:37:56'!
removeAllNext

	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next := nil.
	]! !


!II class methodsFor: 'operations' stamp: 'MF 8/27/2022 22:08:14'!
* aMultiplicand

	^self previous * aMultiplicand + aMultiplicand! !

!II class methodsFor: 'operations' stamp: 'ARM 8/25/2022 21:19:09'!
+ anAdder 

	^self previous + anAdder next! !

!II class methodsFor: 'operations' stamp: 'ds 8/29/2022 20:30:49'!
- subtrahend
	^subtrahend substractFrom: self! !

!II class methodsFor: 'operations' stamp: 'ds 8/30/2022 18:51:29'!
/ aDivisor
	self < aDivisor ifTrue: [self error: self canNotDivideByBiggerNumberErrorDescription ].
	self == aDivisor ifTrue: [^I].
	^self - aDivisor / aDivisor + I ! !

!II class methodsFor: 'operations' stamp: 'ds 8/29/2022 20:38:15'!
substractFrom: minuend
	^minuend previous - self previous
	! !


!II class methodsFor: 'error' stamp: 'ds 8/30/2022 16:11:37'!
canNotDivideByBiggerNumberErrorDescription
	^'Can''t divide by bigger number'! !


!II class methodsFor: 'comparators' stamp: 'ds 8/30/2022 18:50:21'!
< numberToCompare
	numberToCompare == I ifTrue: [^false] ifFalse: [^self previous < numberToCompare previous]! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/30/2022 18:51:59'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'NaturalNumbers'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'NaturalNumbers'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/30/2022 18:51:59'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IV.
	previous := II.! !


!classDefinition: #IV category: 'NaturalNumbers'!
II subclass: #IV
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'NaturalNumbers'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IV class' category: 'NaturalNumbers'!
IV class
	instanceVariableNames: ''!

!IV class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/30/2022 18:51:59'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IV initializeAfterFileIn!