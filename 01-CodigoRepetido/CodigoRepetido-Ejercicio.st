!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 17:25:08'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| timeEllapsed customerBook |
	
	customerBook := CustomerBook new.
	
	timeEllapsed := self measureTime: [customerBook addCustomerNamed: 'John Lennon'].
	
	self assert: timeEllapsed < (50 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 17:25:11'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| timeEllapsed customerBook paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := self createCustomerBookWithCustomer: paulMcCartney .
	
	timeEllapsed := self measureTime: [customerBook removeCustomerNamed: paulMcCartney.].
	
	self assert: timeEllapsed < (100 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ds 9/5/2022 21:54:57'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook caughtError |
			
	customerBook := CustomerBook new.

	caughtError := self do: [customerBook addCustomerNamed: ''] andExpectException: Error.
	self assert: caughtError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
	self assert: customerBook isEmpty ! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ds 9/6/2022 20:20:04'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
	
	johnLennon := 'John Lennon'.	
	customerBook := self createCustomerBookWithCustomer: johnLennon.
	
	self do: [customerBook removeCustomerNamed: 'Paul McCartney'] andExpectException: NotFound. 
	self assertCustomerBook: customerBook hasSingleCustomerNamed: johnLennon.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 18:16:28'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := self createCustomerBookWithCustomer: paulMcCartney.
	
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self assert: 0 equals: customerBook numberOfActiveCustomers.
	self assert: 1 equals: customerBook numberOfSuspendedCustomers.
	self assert: 1 equals: customerBook numberOfCustomers.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 18:21:02'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := self createCustomerBookWithCustomer: paulMcCartney.
	
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assert: customerBook isEmpty.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 17:39:43'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := self createCustomerBookWithCustomer: johnLennon.
	
	self do: [customerBook suspendCustomerNamed: 'George Harrison'] andExpectException: CantSuspend.
	
	self assertCustomerBook: customerBook hasSingleCustomerNamed: johnLennon.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'MF 9/6/2022 17:39:52'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := self createCustomerBookWithCustomer: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self do: [customerBook suspendCustomerNamed: johnLennon] andExpectException: CantSuspend.

	self assertCustomerBook: customerBook hasSingleCustomerNamed: johnLennon.
! !


!CustomerBookTest methodsFor: 'time' stamp: 'ds 9/5/2022 21:27:17'!
measureTime: aClosure	
	| millisecondsBeforeRunning millisecondsAfterRunning |
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	^millisecondsAfterRunning - millisecondsBeforeRunning! !


!CustomerBookTest methodsFor: 'exceptions' stamp: 'MF 9/6/2022 17:13:03'!
do: aClosure andExpectException: anException
	aClosure
		on: anException 
		do: [ :anError | ^anError ].
	self fail.! !


!CustomerBookTest methodsFor: 'initialization' stamp: 'ds 9/5/2022 21:38:32'!
createCustomerBookWithCustomer: aCustomerName
	| customerBook |
	customerBook := CustomerBook new.
	^customerBook addCustomerNamed: aCustomerName.! !


!CustomerBookTest methodsFor: 'asserts' stamp: 'MF 9/6/2022 17:38:13'!
assertCustomerBook: aCustomerBook hasSingleCustomerNamed: aCustomerName
	self assert: aCustomerBook numberOfCustomers = 1.
	self assert: (aCustomerBook includesCustomerNamed: aCustomerName)! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'MF 9/6/2022 17:51:05'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'MF 9/6/2022 18:04:39'!
removeCustomerNamed: aName 
 
	^active remove: aName ifAbsent: [
		 suspended remove: aName ifAbsent: [
			NotFound signal]
		].
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'MF 9/6/2022 18:08:40'!
suspendCustomerNamed: aName 
	
	active remove: aName ifAbsent: [^CantSuspend signal].
	suspended add: aName.
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:12'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
