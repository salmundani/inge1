!classDefinition: #Falso category: 'EjVerdaderoFalso'!
DenotativeObject subclass: #Falso
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjVerdaderoFalso'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Falso class' category: 'EjVerdaderoFalso'!
Falso class
	instanceVariableNames: ''!

!Falso class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:37:15'!
no
	^Verdadero ! !

!Falso class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:46:42'!
o: unBooleano 
	^unBooleano! !

!Falso class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 21:28:24'!
siFalso: aBlockClosure 
	aBlockClosure  value! !

!Falso class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 21:11:18'!
siVerdadero: aBlockClosure 
	^self! !

!Falso class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:43:30'!
y: unBooleano 
	^Falso! !


!classDefinition: #Verdadero category: 'EjVerdaderoFalso'!
DenotativeObject subclass: #Verdadero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjVerdaderoFalso'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Verdadero class' category: 'EjVerdaderoFalso'!
Verdadero class
	instanceVariableNames: ''!

!Verdadero class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:33:38'!
no
	^Falso! !

!Verdadero class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:46:11'!
o: unBooleano 
	^Verdadero.! !

!Verdadero class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 21:29:52'!
siFalso: aBlockClosure 
	^self ! !

!Verdadero class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 21:08:19'!
siVerdadero: aBlockClosure 
	aBlockClosure value! !

!Verdadero class methodsFor: 'protocolo' stamp: 'ds 8/22/2022 20:40:54'!
y: unBooleano 
	^unBooleano ! !


!classDefinition: #VerdaderoFalsoTest category: 'EjVerdaderoFalso'!
DenotativeObject subclass: #VerdaderoFalsoTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EjVerdaderoFalso'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'VerdaderoFalsoTest class' category: 'EjVerdaderoFalso'!
VerdaderoFalsoTest class
	instanceVariableNames: ''!

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:28:41'!
test01VerdaderoNoEsFalso
	Assert that: Verdadero no isEqualTo: Falso! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:36:47'!
test02FalsoNoEsVerdadero
	Assert that: Falso no isEqualTo: Verdadero ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:54:16'!
test03VerdaderoYFalsoEsFalso
	Assert that: (Verdadero y: Falso) isEqualTo: Falso ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:54:32'!
test04VerdaderoYVerdaderoEsVerdadero
	Assert that: (Verdadero y: Verdadero) isEqualTo: Verdadero ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:54:37'!
test05FalsoYFalsoEsFalso
	Assert that: (Falso y: Falso) isEqualTo: Falso ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:54:46'!
test06FalsoYVerdaderoEsFalso
	Assert that: (Falso y: Verdadero) isEqualTo: Falso ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:54:53'!
test07VerdaderoOFalsoEsVerdadero
	Assert that: (Verdadero o: Falso) isEqualTo: Verdadero ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:55:00'!
test08VerdaderoOVerdaderoEsVerdadero
	Assert that: (Verdadero o: Verdadero) isEqualTo: Verdadero ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:55:06'!
test09FalsoOFalsoEsFalso
	Assert that: (Falso o: Falso) isEqualTo: Falso ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 20:55:13'!
test10FalsoOVerdaderoEsVerdadero
	Assert that: (Falso o: Verdadero) isEqualTo: Verdadero ! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 21:32:20'!
test11VerdaderoSiVerdaderoEjecutaElClosure
	|var|
	Verdadero  siVerdadero: [ var := 1 ].
	Assert that: var isEqualTo: 1! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 21:09:59'!
test12FalsoSiVerdaderoNoHaceNada
	|var|
	var := 0.
	Falso siVerdadero: [ var := 1 ].
	Assert that: var isEqualTo: 0! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 21:32:39'!
test13FalsoSiFalsoEjecutaElClosure
	|var|
	Falso  siFalso: [ var := 1 ].
	Assert that: var isEqualTo: 1! !

!VerdaderoFalsoTest class methodsFor: 'as yet unclassified' stamp: 'ds 8/22/2022 21:29:34'!
test14VerdaderoSiFalsoNoHaceNada
	|var|
	var := 0.
	Verdadero siFalso: [ var := 1 ].
	Assert that: var isEqualTo: 0! !
