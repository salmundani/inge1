'From Cuis 6.0 [latest update: #5481] on 29 September 2022 at 9:33:49 pm'!


!String methodsFor: 'baby steps' stamp: 'MF 9/29/2022 21:29:47'!
anagrams
	
	| anagrams index |
	(self size <= 1) ifTrue: [ ^ Set with: self ].
	
	anagrams := Set new.
	
	index := 1.
	[ index <= self size ] whileTrue: [
			
		| aChar subanagrams substring |
		aChar := self at: index.
		substring := self copyWithoutIndex: index.
		subanagrams :=  substring anagrams.
		
		subanagrams do: [ :anagram | anagrams add: (anagram , aChar asString ) ]		.
		index := index + 1.
	].

	^ anagrams
! !

!String methodsFor: 'baby steps' stamp: 'MF 9/29/2022 20:07:58'!
asRot13

	^self collect: [ :aChar | aChar asRot13 ]! !

!String methodsFor: 'baby steps' stamp: 'MF 9/29/2022 21:28:49'!
copyWithoutIndex: index

	index = 1 ifTrue: [ ^self copyFrom: (index + 1) to: self size ].
	index = self size ifTrue: [ ^self copyFrom: 1 to: index - 1 ].
	^(self copyFrom: 1 to: (index - 1)), (self copyFrom: (index + 1) to: self size).! !
