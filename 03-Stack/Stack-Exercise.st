!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test'!
test01FindShouldReturnEmptyCollectionWhenStackIsEmpty
	| stack sentencesWithGivenPrefix|
	stack := OOStack new.	
	sentencesWithGivenPrefix := SentenceFinderByPrefix find: 'Win' in: stack.
	
	self assert: sentencesWithGivenPrefix isEmpty
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'test'!
test02FindShouldRaiseErrorWhenPrefixIsEmpty	
	| aStack |
	
	aStack := OOStack new.
	
	self
		should: [ SentenceFinderByPrefix find: '' in: aStack ]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixEmptyErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'test'!
test03FindShouldRaiseErrorWhenPrefixHasSpaces
	| aStack |
	
	aStack := OOStack new.
	aStack push: 'sentence'.	
	
	self
		should: [ SentenceFinderByPrefix find: 'Win ' in: aStack ]
		raise: Error
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixContainsEmptySpaceErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 9/17/2022 18:51:30'!
test04FindReturnsAllSentencesWithGivenPrefixAndInGivenOrder
	| sentencesWithGivenPrefix expectedSentences |
	sentencesWithGivenPrefix := SentenceFinderByPrefix find: 'Win' in: self stackWithFiveElements.
	expectedSentences := OrderedCollection with: 'Winter is not here' with: 'Winter is here'.
	self assert: sentencesWithGivenPrefix equals: expectedSentences! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 9/17/2022 18:50:47'!
test05FindDoesNotModifyStack
	| stack |
	stack := self stackWithFiveElements.
	SentenceFinderByPrefix find: 'Win' in: self stackWithFiveElements.
	self assert: stack size = 5.
	self assert: stack pop equals: 'Winter is not here'.
	self assert: stack pop equals: 'The winds of Winter'.
	self assert: stack pop equals: 'Winter is here'.
	self assert: stack pop equals: 'winning is everything'.
	self assert: stack pop equals: 'winter is coming'.! !


!SentenceFinderByPrefixTest methodsFor: 'creation' stamp: 'MF 9/17/2022 18:50:34'!
stackWithFiveElements
	| stack |
	stack := OOStack new.
	stack push: 'winter is coming'.
	stack push: 'winning is everything'.
	stack push: 'Winter is here'.
	stack push: 'The winds of Winter'.
	stack push: 'Winter is not here'.
	^stack! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'head'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization'!
initialize
	head := NullNode new! !


!OOStack methodsFor: 'operations'!
isEmpty
	^head isNull! !

!OOStack methodsFor: 'operations'!
pop
	| value |
	value := head value.
	head := head child.
	^value! !

!OOStack methodsFor: 'operations'!
push: anElement
	| node |
	node := ValueNode new: anElement withChild: head.
	head := node
	! !

!OOStack methodsFor: 'operations'!
size
	^head numberOfSubnodes! !

!OOStack methodsFor: 'operations'!
top
	^head value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!OOStack class methodsFor: 'instance creation'!
new
	^self basicNew initialize! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error descriptions'!
prefixContainsEmptySpaceErrorDescription
	^'prefix must not contain empty space characters'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions'!
prefixEmptyErrorDescription
	^ 'prefix must not be empty'! !


!SentenceFinderByPrefix class methodsFor: 'find'!
find: aPrefix in: aSentencesStack
	|sentencesWithGivenPrefix sentencesPoppedFromStack |
	
	self validatePrefix: aPrefix.
	
	sentencesWithGivenPrefix := OrderedCollection new.	
	"Se podria usar un OOStack aca pero no podriamos hacer el 'do:' en rebuildStack para rearmarlo mas facilmente, no sabemos si se esperaba que se haga asi"
	sentencesPoppedFromStack := OrderedCollection new.	
	
	[aSentencesStack isEmpty]
		whileFalse: [
			| aSentence |
			aSentence := aSentencesStack pop.
			(aSentence beginsWith: aPrefix) ifTrue: [ sentencesWithGivenPrefix add: aSentence ].
			sentencesPoppedFromStack addFirst: aSentence
		].	

	self rebuildStack: aSentencesStack with: sentencesPoppedFromStack.
	^sentencesWithGivenPrefix! !


!SentenceFinderByPrefix class methodsFor: 'auxiliar' stamp: 'MF 9/17/2022 18:57:35'!
rebuildStack: aStack with: aSentencesCollection
	aSentencesCollection do: [ :sentence | aStack push: sentence ]! !


!SentenceFinderByPrefix class methodsFor: 'validation' stamp: 'MF 9/17/2022 18:22:05'!
validatePrefix: aPrefix
	aPrefix isEmpty ifTrue: [ self error: self prefixEmptyErrorDescription ].
	(aPrefix includesSubString: ' ') ifTrue: [ self error: self prefixContainsEmptySpaceErrorDescription ]! !


!classDefinition: #StackNode category: 'Stack-Exercise'!
Object subclass: #StackNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackNode methodsFor: 'observers'!
child
	self subclassResponsibility! !

!StackNode methodsFor: 'observers'!
isNull
	self subclassResponsibility! !

!StackNode methodsFor: 'observers'!
numberOfSubnodes
	self subclassResponsibility! !

!StackNode methodsFor: 'observers'!
value
	self subclassResponsibility! !


!classDefinition: #NullNode category: 'Stack-Exercise'!
StackNode subclass: #NullNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NullNode methodsFor: 'observers'!
child
	self raiseError! !

!NullNode methodsFor: 'observers'!
isNull
	^true! !

!NullNode methodsFor: 'observers'!
numberOfSubnodes
	^0! !

!NullNode methodsFor: 'observers'!
raiseError
	self error: OOStack stackEmptyErrorDescription! !

!NullNode methodsFor: 'observers'!
value
	self raiseError! !


!classDefinition: #ValueNode category: 'Stack-Exercise'!
StackNode subclass: #ValueNode
	instanceVariableNames: 'value child'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!ValueNode methodsFor: 'initialization'!
initializeWith: initValue child: initChild
	value := initValue.
	child := initChild.! !


!ValueNode methodsFor: 'observers'!
child
	^child! !

!ValueNode methodsFor: 'observers'!
isNull
	^false! !

!ValueNode methodsFor: 'observers'!
numberOfSubnodes
	^1 + child numberOfSubnodes! !

!ValueNode methodsFor: 'observers'!
value
	^value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ValueNode class' category: 'Stack-Exercise'!
ValueNode class
	instanceVariableNames: ''!

!ValueNode class methodsFor: 'instance creation'!
new: value withChild: child
	^self basicNew initializeWith: value child: child. ! !
