!classDefinition: #NaturalNumbersTester category: 'Roman Numbers Tests'!
DenotativeObject subclass: #NaturalNumbersTester
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Roman Numbers Tests'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NaturalNumbersTester class' category: 'Roman Numbers Tests'!
NaturalNumbersTester class
	instanceVariableNames: ''!

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:50:51'!
test01OnePlusAnAdderEqualsTheNextOfTheAdder

	Assert that: I + II isEqualTo: II next.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:50:58'!
test02NumbersBiggerThanOneCanBeAdded

	Assert that: II + II isEqualTo: IV! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:51:06'!
test03ANumberMinusOneEqualsThePreviousOfTheNumber

	Assert that: II - I isEqualTo: II previous.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:51:13'!
test04NumbersBiggerThanOneCanBeSubstracted

	Assert that: IV - II isEqualTo: II
! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:51:21'!
test05SubstractionDoesNotSupportNegativeNumbers

	Assert should: [ I - II ]
		signal: Error
		withDescription: I negativeNumbersNotSupportedErrorDescription.
! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:11'!
test06ANumberMultipliedByOneEqualsANumber

	Assert that: II * I isEqualTo: II.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:18'!
test07NumbersBiggerThanOneCanBeMultiplied

	Assert that: II * II isEqualTo: IV! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:26'!
test08OneMultipliedByOneEqualsOne

	Assert that: I * I isEqualTo: I.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:32'!
test09NumbersBiggerThanOneCanBeDivided

	Assert that: IV / II isEqualTo: II
! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:41'!
test10ANumberDividedByOneEqualsItself

	Assert that: II / I isEqualTo: II.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:53:48'!
test11OneDividedByOneEqualsOne

	Assert that: I / I isEqualTo: I.! !

!NaturalNumbersTester class methodsFor: 'testing' stamp: 'ds 8/30/2022 17:54:05'!
test12CanNotDivideByABiggerNumber

	Assert should: [ III /  IV ]
		signal: Error
		withDescription: II canNotDivideByBiggerNumberErrorDescription.
	
	Assert should: [ I /  II]
		signal: Error
		withDescription: II canNotDivideByBiggerNumberErrorDescription
! !
