!classDefinition: #SeñalDeAvanceDeCorrientes category: 'Semaforo'!
Light subclass: #SeñalDeAvanceDeCorrientes
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDeAvanceDeCorrientes class' category: 'Semaforo'!
SeñalDeAvanceDeCorrientes class
	instanceVariableNames: ''!

!SeñalDeAvanceDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:17:31'!
apagar
	self color: Color black! !

!SeñalDeAvanceDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:17:31'!
prender
	self color: Color green! !


!SeñalDeAvanceDeCorrientes class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeñalDeAvanceDeMaipu category: 'Semaforo'!
Light subclass: #SeñalDeAvanceDeMaipu
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDeAvanceDeMaipu class' category: 'Semaforo'!
SeñalDeAvanceDeMaipu class
	instanceVariableNames: ''!


!SeñalDeAvanceDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:45:51'!
apagar
	self color: Color black! !

!SeñalDeAvanceDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:46:08'!
prender
	self color: Color green! !


!SeñalDeAvanceDeMaipu class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeñalDeDetencionDeCorrientes category: 'Semaforo'!
Light subclass: #SeñalDeDetencionDeCorrientes
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDeDetencionDeCorrientes class' category: 'Semaforo'!
SeñalDeDetencionDeCorrientes class
	instanceVariableNames: ''!

!SeñalDeDetencionDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:17:56'!
apagar
	self color: Color black! !

!SeñalDeDetencionDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:17:56'!
prender
	self color: Color red! !


!SeñalDeDetencionDeCorrientes class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeñalDeDetencionDeMaipu category: 'Semaforo'!
Light subclass: #SeñalDeDetencionDeMaipu
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDeDetencionDeMaipu class' category: 'Semaforo'!
SeñalDeDetencionDeMaipu class
	instanceVariableNames: ''!

!SeñalDeDetencionDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:45:31'!
apagar
	self color: Color black! !

!SeñalDeDetencionDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:45:39'!
prender
	self color: Color red! !


!SeñalDeDetencionDeMaipu class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeñalDePrecaucionDeCorrientes category: 'Semaforo'!
Light subclass: #SeñalDePrecaucionDeCorrientes
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDePrecaucionDeCorrientes class' category: 'Semaforo'!
SeñalDePrecaucionDeCorrientes class
	instanceVariableNames: ''!

!SeñalDePrecaucionDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:16:47'!
apagar
	self color: Color black! !

!SeñalDePrecaucionDeCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:16:47'!
prender
	self color: Color yellow! !


!SeñalDePrecaucionDeCorrientes class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeñalDePrecaucionDeMaipu category: 'Semaforo'!
Light subclass: #SeñalDePrecaucionDeMaipu
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeñalDePrecaucionDeMaipu class' category: 'Semaforo'!
SeñalDePrecaucionDeMaipu class
	instanceVariableNames: ''!

!SeñalDePrecaucionDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:42:09'!
apagar
	self color: Color black! !

!SeñalDePrecaucionDeMaipu class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:41:51'!
prender
	self color: Color yellow! !


!SeñalDePrecaucionDeMaipu class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #ReguladorDeTransitoDeCorrientes category: 'Semaforo'!
ThreeLightsContainer subclass: #ReguladorDeTransitoDeCorrientes
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ReguladorDeTransitoDeCorrientes class' category: 'Semaforo'!
ReguladorDeTransitoDeCorrientes class
	instanceVariableNames: ''!

!ReguladorDeTransitoDeCorrientes class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #ReguladorDeTransitoDeMaipu category: 'Semaforo'!
ThreeLightsContainer subclass: #ReguladorDeTransitoDeMaipu
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ReguladorDeTransitoDeMaipu class' category: 'Semaforo'!
ReguladorDeTransitoDeMaipu class
	instanceVariableNames: ''!

!ReguladorDeTransitoDeMaipu class methodsFor: '--** private fileout/in **--' stamp: 'ds 8/29/2022 20:04:52'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SemaforoDeMaipuYCorrientes category: 'Semaforo'!
DenotativeObject subclass: #SemaforoDeMaipuYCorrientes
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SemaforoDeMaipuYCorrientes class' category: 'Semaforo'!
SemaforoDeMaipuYCorrientes class
	instanceVariableNames: ''!

!SemaforoDeMaipuYCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:06:01'!
avisarPrecaucionPorEncendido
	5 timesRepeat: [ self titilarLuzAmarilla ]! !

!SemaforoDeMaipuYCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 18:59:46'!
prender
	self avisarPrecaucionPorEncendido.
	! !

!SemaforoDeMaipuYCorrientes class methodsFor: 'as yet unclassified' stamp: 'ds 8/29/2022 19:41:46'!
titilarLuzAmarilla
	ReguladorDeTransitoDeCorrientes titilarLuzAmarilla.
	ReguladorDeTransitoDeMaipu titilarLuzAmarilla.! !

SeñalDeAvanceDeCorrientes initializeAfterFileIn!
SeñalDeAvanceDeMaipu initializeAfterFileIn!
SeñalDeDetencionDeCorrientes initializeAfterFileIn!
SeñalDeDetencionDeMaipu initializeAfterFileIn!
SeñalDePrecaucionDeCorrientes initializeAfterFileIn!
SeñalDePrecaucionDeMaipu initializeAfterFileIn!
ReguladorDeTransitoDeCorrientes initializeAfterFileIn!
ReguladorDeTransitoDeMaipu initializeAfterFileIn!