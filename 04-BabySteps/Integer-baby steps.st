'From Cuis 6.0 [latest update: #5481] on 29 September 2022 at 9:34:05 pm'!


!Integer methodsFor: 'baby steps' stamp: 'MF 9/29/2022 19:34:04'!
factorize
	self = 1 ifTrue: [ ^ Bag new ].
	
	2 to: self do: [ :aNumber | 
		(self isDivisibleBy: aNumber) ifTrue: [ 
			^(Bag with: aNumber) addAll: (self / aNumber ) factorize; yourself
		] 
	].! !
