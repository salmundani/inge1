!classDefinition: #XXX category: 'Collections-Excercise'!
Object subclass: #XXX
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Collections-Excercise'!

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:34:02'!
extracVowels
	^'Hola' select: [ :char | char isVowel ]! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:12:55'!
findDoubles 
	|elements|
	elements := #(1 2 5 6 9).
	
	elements collect: [:each | each * 3].
	
	^elements collect: [:each | each * 2].! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:20:53'!
findFirstEven
	
	|elements|
	
	elements := #(1 2 5 6 9).
	
	^elements detect: [:each | each even]! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:21:03'!
findFirstEvenInexistant
	
	|elements|
	
	elements := #(1).
	
	^elements detect: [:each | each even] ifNone: [self error: 'No hay pares']! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 20:58:14'!
findOddsPart0
	
	|elements index odds|
	
	elements := #(1 2 5 6 9).
	
	odds := OrderedCollection new.
	index := 1.
	
	[index <= elements size]
	whileTrue: [
		((elements at: index) odd) ifTrue: [odds add:(elements at: index)].
		index := index + 1.
		].
	^odds! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:00:32'!
findOddsPart1
	
	|elements odds|
	
	elements := #(1 2 5 6 9).
	
	odds := OrderedCollection new.
	
	elements do: [:each | 
		each odd ifTrue: [
			odds add: each]].
	^odds! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:02:48'!
findOddsPart2
	
	|elements|
	
	elements := #(1 2 5 6 9).
	
	^elements select: [:each | each odd].
	! !

!XXX methodsFor: 'as yet unclassified' stamp: 'MF 9/1/2022 21:32:05'!
sumElementsInjecting
	^#(1 5 9) inject: 0 into: [ :sum :element | sum + element ]! !
